-- v4_skus_dev <- variants
insert into v4_skus_dev (
client_id,
id,
bar_code,
variant_id,
registered_at,
last_updated_at,
deleted_at
)
select
p.ec_id,
v.id + 1000000,
v.code,
v.id,
v.created_at,
v.updated_at,
v.deleted_at
from variants as v
left join products as p on v.product_id = p.id
where
p.ec_id in (32,471,527,580,596,696,718,721,744,774,779,780,781,782,808,813,814,829,838) and  -- 有効なEC（2020.8.26時点確認）
p.deleted_at is null and v.deleted_at is null
;
