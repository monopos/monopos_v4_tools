-- v4_variant_stocks <- variant_places
insert into v4_variant_stocks (
client_id,
id,
place_id,
variant_id,
total_book_stocks,
overall_normal_stocks,
allocated_normal_stocks,
reserved_normal_stocks,
in_transit_normal_stocks,
overall_irregular_stocks,
allocated_irregular_stocks,
in_transit_irregular_stocks,
overall_unknown_stocks,
allocated_unknown_stocks,
in_transit_unknown_stocks,
overall_lost_stocks,
created_at
)
select
p.ec_id, -- client_id
min(s.id), -- id
s.place_id, -- place_id
s.variant_id, -- variant_id
sum(s.stock), -- total_book_stocks
sum(s.stock), -- overall_normal_stocks
0, -- allocated_normal_stocks
0, -- reserved_normal_stocks
0, -- in_transit_normal_stocks
0, -- overall_irregular_stocks
0, -- allocated_irregular_stocks
0, -- in_transit_irregular_stocks
0, -- overall_unknown_stocks
0, -- allocated_unknown_stocks
0, -- in_transit_unknown_stocks
0, -- overall_lost_stocks
coalesce(nullif(min(s.created_at),'0000-00-00 00:00:00'),nullif(max(s.created_at),'0000-00-00 00:00:00'),nullif(min(s.updated_at),'0000-00-00 00:00:00'),nullif(max(s.updated_at),'0000-00-00 00:00:00')) -- created_at
from variant_places as s
inner join variants as v on (s.variant_id = v.id and v.deleted_at is null)
inner join products as p on (v.product_id = p.id and p.deleted_at is null)
inner join places as l on (s.place_id = l.id and l.deleted_at is null)
where
p.ec_id in (32,471,527,580,596,696,718,721,744,774,779,780,781,782,808,813,814,829,838) and  -- 有効なEC（2020.8.26時点確認）
s.deleted_at is null
group by
s.place_id, s.variant_id
;


-- v4_sku_stocks <- variant_places
insert into v4_sku_stocks (
client_id,
id,
variant_stock_id,
place_id,
sku_id,
received_date,
total_book_stocks,
overall_normal_stocks,
allocated_normal_stocks,
reserved_normal_stocks,
in_transit_normal_stocks,
overall_irregular_stocks,
allocated_irregular_stocks,
in_transit_irregular_stocks,
overall_unknown_stocks,
allocated_unknown_stocks,
in_transit_unknown_stocks,
overall_lost_stocks,
created_at
)
select
p.ec_id, -- client_id
s.id + 1000000, -- id
z.id, -- variant_stock_id
s.place_id, -- place_id
s.variant_id + 1000000, -- sku_id
s.created_at, -- received_date
s.stock, -- total_book_stocks
s.stock, -- overall_normal_stocks
0, -- allocated_normal_stocks
0, -- reserved_normal_stocks
0, -- in_transit_normal_stocks
0, -- overall_irregular_stocks
0, -- allocated_irregular_stocks
0, -- in_transit_irregular_stocks
0, -- overall_unknown_stocks
0, -- allocated_unknown_stocks
0, -- in_transit_unknown_stocks
0, -- overall_lost_stocks
coalesce(nullif(s.created_at,'0000-00-00 00:00:00'),nullif(s.updated_at,'0000-00-00 00:00:00')) -- created_at
from variant_places as s
inner join variants as v on (s.variant_id = v.id and v.deleted_at is null)
inner join products as p on (v.product_id = p.id and p.deleted_at is null)
inner join places as l on (s.place_id = l.id and l.deleted_at is null)
inner join v4_variant_stocks as z on (s.place_id = z.place_id and s.variant_id = z.variant_id)
where
p.ec_id in (32,471,527,580,596,696,718,721,744,774,779,780,781,782,808,813,814,829,838) and  -- 有効なEC（2020.8.26時点確認）
s.deleted_at is null
;


-- 検査
select
vs.place_id,
vs.variant_id,
vs.total_book_stocks,
sum(ss.total_book_stocks),
count(ss.total_book_stocks)
from v4_variant_stocks as vs
left join v4_sku_stocks as ss on vs.id = ss.variant_stock_id
group by vs.place_id,vs.variant_id
having vs.total_book_stocks <> sum(ss.total_book_stocks)
;


select
vs.place_id,
vs.variant_id,
vs.total_book_stocks,
ss.sku_id,
ss.received_date,
ss.total_book_stocks
from v4_variant_stocks as vs
left join v4_sku_stocks as ss on vs.id = ss.variant_stock_id
where vs.id in (
  select
  svs.id
  from v4_variant_stocks as svs
  left join v4_sku_stocks as sss on svs.id = sss.variant_stock_id
  group by svs.place_id,svs.variant_id
  having count(sss.total_book_stocks) > 1
)
order by
vs.place_id,
vs.variant_id
;



