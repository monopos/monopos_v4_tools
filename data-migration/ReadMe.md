# **データ移行スクリプト**

## **含まれるもの**

- "[migrate_v4_skus_dev.sql](https://bitbucket.org/monopos/monopos_v4_tools/src/master/data-migration/migrate_v4_skus_dev.sql)" ... 既存のバリアントマスター(variants)を元に、SKUマスター〔開発版〕(v4_skus_dev)を生成するスクリプト

- "[migrate_v4_variant_and_sku_stocks.sql](https://bitbucket.org/monopos/monopos_v4_tools/src/master/data-migration/migrate_v4_variant_and_sku_stocks.sql)" ... 既存のvariant_placesテーブルの内容を、バリアント在庫数(v4_variant_stocks)とSKU在庫数(v4_sku_stocks)へデータ移行するスクリプト

## **"migrate_v4_skus_dev"データ移行ポリシー**

- 既存のvariantsに対して、一対一でv4_sku_devを生成する。
- 生成するv4_sku_devのidは、対応する元となるvariants.idに1000000を加算したものとする。（なお、v4_skus_devテーブルのAUTO INCREMENTの開始値は2000000としている。）

## **"migrate_v4_variant_and_sku_stocks"データ移行ポリシー**

o **v4_variant_stocks**

- 生成するv4_variant_stocksのidは、対応する元となるvariant_places.idと同じ値とする。（なお、v4_variant_stocksテーブルのAUTO INCREMENTの開始値は2000000とする予定。（※2020年9月15日時点））

o **v4_sku_stocks**

- 生成するv4_sku_stocksのidは、対応する元となるvariant_places.idに1000000を加算したものとする。（なお、v4_sku_stocksテーブルのAUTO INCREMENTの開始値は2000000とする予定。（※2020年9月15日時点））
- FKとしてsku_idを持つが、セットする値は、「"migrate_v4_skus_dev"データ移行ポリシー」にあるように、元となるvariant_idの値に1000000を加算したものとしている。

## **"migrate_v4_skus_dev"実行時の注意**

移行元のvariantsテーブルに、移行先のv4_skus_devテーブルのユニーク制約に引っかかる重複データ（※client_id, bar_codeが同じ値のレコード）が存在する場合があります。

ユニーク制約違反で移行に失敗した場合、次の対処をして、スクリプトを再実行してください。

＜開発環境の場合＞

・移行元の重複データのいずれかのdeleted_atに日付をセットして抹消状態としてください。

＜本番環境の場合＞

T.B.D.

## **"migrate_v4_variant_and_sku_stocks"実行時の注意**

移行元のvariant_placesテーブルに、移行先のv4_sku_stocksテーブルのユニーク制約に引っかかる重複データ（※place_id, variant_id, created_atが同じ値のレコード）が存在する場合があります。

ユニーク制約違反で移行に失敗した場合、次の対処をして、スクリプトを再実行してください。

＜開発環境の場合＞

・移行元の重複データのいずれかを物理削除してください。

＜本番環境の場合＞

・片方を削除すればいいのかもしれないし、日付をずらしてあげるべきかもしれません。いずれにせよ、実データの実際の様子次第で個別に判断する必要があります。
