# **データベース・スキーマ定義とERD**

データベース・スキーマを開発・保守するためのツールキットです。スキーマ定義とERDが含まれます。

ここに含まれるものは「DBeaverプロジェクト」として構成されています。運用手順について下記を参照してください。

* https://monopos.atlassian.net/wiki/spaces/MON/pages/817594427/

## **DBeaverをセットアップせず参照できるもの**

下記については、DBeaverをセットアップせずとも直ちに参照できます。

#### ■ スキーマ定義全体のMySQLダンプファイル

"V4"としてのスキーマ定義全体を含む、MySQLのデータベースダンプファイルが、下記にあります。

* https://bitbucket.org/monopos/monopos_v4_tools/src/master/monopos-db/Dump/

#### ■ .pngにエクスポートしたERD

.pngにエクスポートしたERDが下記にあります。

* https://bitbucket.org/monopos/monopos_v4_tools/src/master/monopos-db/Diagrams/
