-- MySQL dump 10.13  Distrib 5.7.22, for macos10.13 (x86_64)
--
-- Host: 127.0.0.1    Database: monoposdb_v4
-- ------------------------------------------------------
-- Server version	5.6.46

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `places`
--

DROP TABLE IF EXISTS `places`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `places` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ec_id` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order` int(10) unsigned NOT NULL DEFAULT '0',
  `description` text COLLATE utf8_unicode_ci,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `priority` int(10) unsigned NOT NULL DEFAULT '0',
  `sellable` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `places_id_status_order_index` (`id`,`status`,`order`) USING BTREE,
  KEY `places_id_status_priority_sellable_index` (`id`,`status`,`priority`,`sellable`) USING BTREE,
  KEY `places_name_index` (`name`) USING BTREE,
  KEY `stock_query` (`status`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `places`
--

LOCK TABLES `places` WRITE;
/*!40000 ALTER TABLE `places` DISABLE KEYS */;
/*!40000 ALTER TABLE `places` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ec_id` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `display_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `url_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `blurb` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `brand_id` bigint(20) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `subcategory_id` int(10) unsigned NOT NULL,
  `gender` int(10) unsigned NOT NULL DEFAULT '0',
  `condition` int(10) unsigned NOT NULL DEFAULT '0',
  `season_code` bigint(20) unsigned NOT NULL DEFAULT '0',
  `price` decimal(12,2) NOT NULL,
  `is_subject_to_reduced_tax` tinyint(1) NOT NULL DEFAULT '0' COMMENT '軽減税率対象か？',
  `tax_type` int(11) NOT NULL DEFAULT '0',
  `country_of_origin` varchar(7) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `point_ratio` int(10) unsigned NOT NULL DEFAULT '1',
  `released_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `priority` int(10) unsigned NOT NULL DEFAULT '1',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `base_status` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `off_rate` double NOT NULL DEFAULT '0',
  `sale_price` int(11) NOT NULL DEFAULT '0',
  `product_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '品番',
  `possible_periodical_order` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `product_brand_status_index` (`deleted_at`,`brand_id`,`status`) USING BTREE,
  KEY `products_brand_id_index` (`brand_id`) USING BTREE,
  KEY `products_category_id_index` (`category_id`) USING BTREE,
  KEY `products_ec_id_product_number_index` (`ec_id`,`product_number`) USING BTREE,
  KEY `products_priority_status_released_at_index` (`priority`,`status`,`released_at`) USING BTREE,
  KEY `products_subcategory_id_index` (`subcategory_id`) USING BTREE,
  KEY `products_url_path_index` (`url_path`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shipping_informations`
--

DROP TABLE IF EXISTS `shipping_informations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shipping_informations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `place_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '宛名',
  `phone_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '電話番号',
  `postcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '郵便番号',
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '住所',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `shipping_information_place_id_unq` (`place_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipping_informations`
--

LOCK TABLES `shipping_informations` WRITE;
/*!40000 ALTER TABLE `shipping_informations` DISABLE KEYS */;
/*!40000 ALTER TABLE `shipping_informations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `v4_places`
--

DROP TABLE IF EXISTS `v4_places`;
/*!50001 DROP VIEW IF EXISTS `v4_places`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v4_places` AS SELECT 
 1 AS `client_id`,
 1 AS `id`,
 1 AS `name`,
 1 AS `description`,
 1 AS `postal_code`,
 1 AS `address`,
 1 AS `address_name`,
 1 AS `contact_phone_number`,
 1 AS `registered_at`,
 1 AS `last_updated_at`,
 1 AS `deleted_at`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `v4_sku_stocks`
--

DROP TABLE IF EXISTS `v4_sku_stocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `v4_sku_stocks` (
  `client_id` bigint(20) NOT NULL COMMENT 'クライアントID',
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'SKU在庫数ID',
  `variant_stock_id` bigint(20) NOT NULL COMMENT 'バリアント在庫数ID',
  `place_id` bigint(20) NOT NULL COMMENT '場所ID',
  `sku_id` bigint(20) NOT NULL COMMENT 'SKU ID',
  `received_date` date NOT NULL COMMENT '入荷日',
  `total_book_stocks` int(11) NOT NULL COMMENT '総理論在庫数（導出項目）',
  `overall_normal_stocks` int(11) NOT NULL COMMENT '通常品全理論在庫数',
  `allocated_normal_stocks` int(11) NOT NULL COMMENT '通常品引当済数',
  `reserved_normal_stocks` int(11) NOT NULL COMMENT '通常品取置数',
  `in_transit_normal_stocks` int(11) NOT NULL COMMENT '通常品積送中数',
  `overall_irregular_stocks` int(11) NOT NULL COMMENT '不良品全理論在庫数',
  `allocated_irregular_stocks` int(11) NOT NULL COMMENT '不良品引当済数',
  `in_transit_irregular_stocks` int(11) NOT NULL COMMENT '不良品積送中数',
  `overall_unknown_stocks` int(11) NOT NULL COMMENT '品質不明品全理論在庫数',
  `allocated_unknown_stocks` int(11) NOT NULL COMMENT '品質不明品引当済数',
  `in_transit_unknown_stocks` int(11) NOT NULL COMMENT '品質不明品積送中数',
  `overall_lost_stocks` int(11) NOT NULL COMMENT '所在不明品全理論在庫数',
  `created_at` datetime(6) NOT NULL COMMENT '作成日時',
  `record_timestamp` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '【システム項目】レコードタイムスタンプ',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_sku_stocks` (`place_id`,`sku_id`,`received_date`),
  KEY `index_sku_stocks_1` (`variant_stock_id`) USING BTREE,
  KEY `index_sku_stocks_2` (`place_id`) USING BTREE,
  KEY `index_sku_stocks_3` (`sku_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2000000 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='SKU在庫数';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `v4_sku_stocks`
--

LOCK TABLES `v4_sku_stocks` WRITE;
/*!40000 ALTER TABLE `v4_sku_stocks` DISABLE KEYS */;
/*!40000 ALTER TABLE `v4_sku_stocks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `v4_skus`
--

DROP TABLE IF EXISTS `v4_skus`;
/*!50001 DROP VIEW IF EXISTS `v4_skus`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v4_skus` AS SELECT 
 1 AS `client_id`,
 1 AS `id`,
 1 AS `bar_code`,
 1 AS `variant_id`,
 1 AS `registered_at`,
 1 AS `last_updated_at`,
 1 AS `deleted_at`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `v4_skus_dev`
--

DROP TABLE IF EXISTS `v4_skus_dev`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `v4_skus_dev` (
  `client_id` bigint(20) NOT NULL COMMENT 'クライアントID',
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'SKU ID',
  `bar_code` varchar(100) COLLATE utf8mb4_bin NOT NULL COMMENT 'バーコード',
  `variant_id` bigint(20) NOT NULL COMMENT 'バリアントID',
  `registered_at` datetime(6) NOT NULL COMMENT '登録日時',
  `last_updated_at` datetime(6) NOT NULL COMMENT '最終更新日時',
  `deleted_at` datetime(6) DEFAULT NULL COMMENT '抹消日時',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_skus_dev` (`client_id`,`bar_code`)
) ENGINE=InnoDB AUTO_INCREMENT=2000000 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='SKUマスター';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `v4_skus_dev`
--

LOCK TABLES `v4_skus_dev` WRITE;
/*!40000 ALTER TABLE `v4_skus_dev` DISABLE KEYS */;
/*!40000 ALTER TABLE `v4_skus_dev` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `v4_skus_prd`
--

DROP TABLE IF EXISTS `v4_skus_prd`;
/*!50001 DROP VIEW IF EXISTS `v4_skus_prd`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v4_skus_prd` AS SELECT 
 1 AS `client_id`,
 1 AS `id`,
 1 AS `bar_code`,
 1 AS `variant_id`,
 1 AS `registered_at`,
 1 AS `last_updated_at`,
 1 AS `deleted_at`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `v4_stock_adjustment_details`
--

DROP TABLE IF EXISTS `v4_stock_adjustment_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `v4_stock_adjustment_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '在庫調整明細ID',
  `stock_adjustment_id` bigint(20) NOT NULL COMMENT '在庫調整ID',
  `sku_id` bigint(20) NOT NULL COMMENT 'SKU ID',
  `stock_condition` char(20) COLLATE utf8mb4_bin NOT NULL COMMENT '在庫品質',
  `adjustment_quantity` int(11) NOT NULL COMMENT '調整数',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_stock_adjustment_details` (`stock_adjustment_id`,`sku_id`,`stock_condition`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='在庫調整明細';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `v4_stock_adjustment_details`
--

LOCK TABLES `v4_stock_adjustment_details` WRITE;
/*!40000 ALTER TABLE `v4_stock_adjustment_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `v4_stock_adjustment_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `v4_stock_adjustment_tags`
--

DROP TABLE IF EXISTS `v4_stock_adjustment_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `v4_stock_adjustment_tags` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '在庫調整タグID',
  `stock_adjustment_id` bigint(20) NOT NULL COMMENT '在庫調整ID',
  `tag_id` bigint(20) NOT NULL COMMENT 'タグID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_stock_adjustment_tags` (`stock_adjustment_id`,`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='在庫調整タグ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `v4_stock_adjustment_tags`
--

LOCK TABLES `v4_stock_adjustment_tags` WRITE;
/*!40000 ALTER TABLE `v4_stock_adjustment_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `v4_stock_adjustment_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `v4_stock_adjustments`
--

DROP TABLE IF EXISTS `v4_stock_adjustments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `v4_stock_adjustments` (
  `client_id` bigint(20) NOT NULL COMMENT 'クライアントID',
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '在庫調整ID',
  `issued_at` datetime(6) NOT NULL COMMENT '起票日時',
  `issued_by` bigint(20) NOT NULL COMMENT '起票者',
  `place_id` bigint(20) NOT NULL COMMENT '場所ID',
  `adjusted_at` datetime(6) NOT NULL COMMENT '調整日時',
  `adjustment_class` char(20) COLLATE utf8mb4_bin NOT NULL COMMENT '調整区分',
  `reason` text COLLATE utf8mb4_bin NOT NULL COMMENT '事由',
  `record_timestamp` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '【システム項目】レコードタイムスタンプ',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_stock_adjustments` (`issued_at`,`place_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='在庫調整';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `v4_stock_adjustments`
--

LOCK TABLES `v4_stock_adjustments` WRITE;
/*!40000 ALTER TABLE `v4_stock_adjustments` DISABLE KEYS */;
/*!40000 ALTER TABLE `v4_stock_adjustments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `v4_stock_change_logs`
--

DROP TABLE IF EXISTS `v4_stock_change_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `v4_stock_change_logs` (
  `client_id` bigint(20) NOT NULL COMMENT 'クライアントID',
  `place_id` bigint(20) NOT NULL COMMENT '場所ID',
  `sku_id` bigint(20) NOT NULL COMMENT 'SKU ID',
  `stock_condition` char(20) COLLATE utf8mb4_bin NOT NULL COMMENT '在庫品質',
  `received_date` date NOT NULL COMMENT '入荷日',
  `changed_at` datetime(6) NOT NULL COMMENT '変動発生日時',
  `changed_by` bigint(20) NOT NULL COMMENT '変動発生者',
  `change_class` char(50) COLLATE utf8mb4_bin NOT NULL COMMENT '変動区分',
  `manipulation_class` char(50) COLLATE utf8mb4_bin NOT NULL COMMENT '操作区分',
  `overall_book_stocks_changes` int(11) NOT NULL COMMENT '全理論在庫数増減',
  `allocated_stocks_changes` int(11) NOT NULL COMMENT '引当済在庫数増減',
  `reserved_stocks_changes` int(11) NOT NULL COMMENT '取置在庫数増減',
  `in_transit_stocks_changes` int(11) NOT NULL COMMENT '積送中在庫数増減',
  `record_timestamp` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '【システム項目】レコードタイムスタンプ',
  PRIMARY KEY (`place_id`,`sku_id`,`stock_condition`,`received_date`,`changed_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='在庫変動記録';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `v4_stock_change_logs`
--

LOCK TABLES `v4_stock_change_logs` WRITE;
/*!40000 ALTER TABLE `v4_stock_change_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `v4_stock_change_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `v4_variant_stocks`
--

DROP TABLE IF EXISTS `v4_variant_stocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `v4_variant_stocks` (
  `client_id` bigint(20) NOT NULL COMMENT 'クライアントID',
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'バリアント在庫数ID',
  `place_id` bigint(20) NOT NULL COMMENT '場所ID',
  `variant_id` bigint(20) NOT NULL COMMENT 'バリアントID',
  `total_book_stocks` int(11) NOT NULL COMMENT '総理論在庫数（導出項目）',
  `overall_normal_stocks` int(11) NOT NULL COMMENT '通常品全理論在庫数（導出項目）',
  `allocated_normal_stocks` int(11) NOT NULL COMMENT '通常品引当済数',
  `reserved_normal_stocks` int(11) NOT NULL COMMENT '通常品取置数（導出項目）',
  `in_transit_normal_stocks` int(11) NOT NULL COMMENT '通常品積送中数（導出項目）',
  `overall_irregular_stocks` int(11) NOT NULL COMMENT '不良品全理論在庫数（導出項目）',
  `allocated_irregular_stocks` int(11) NOT NULL COMMENT '不良品引当済数',
  `in_transit_irregular_stocks` int(11) NOT NULL COMMENT '不良品積送中数（導出項目）',
  `overall_unknown_stocks` int(11) NOT NULL COMMENT '品質不明品全理論在庫数（導出項目）',
  `allocated_unknown_stocks` int(11) NOT NULL COMMENT '品質不明品引当済数',
  `in_transit_unknown_stocks` int(11) NOT NULL COMMENT '品質不明品積送中数（導出項目）',
  `overall_lost_stocks` int(11) NOT NULL COMMENT '所在不明品全理論在庫数（導出項目）',
  `created_at` datetime(6) NOT NULL COMMENT '作成日時',
  `record_timestamp` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '【システム項目】レコードタイムスタンプ',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_variant_stocks` (`place_id`,`variant_id`),
  KEY `index_variant_stocks_1` (`place_id`) USING BTREE,
  KEY `index_variant_stocks_2` (`variant_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2000000 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='バリアント在庫数';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `v4_variant_stocks`
--

LOCK TABLES `v4_variant_stocks` WRITE;
/*!40000 ALTER TABLE `v4_variant_stocks` DISABLE KEYS */;
/*!40000 ALTER TABLE `v4_variant_stocks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `v4_variants`
--

DROP TABLE IF EXISTS `v4_variants`;
/*!50001 DROP VIEW IF EXISTS `v4_variants`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v4_variants` AS SELECT 
 1 AS `client_id`,
 1 AS `id`,
 1 AS `product_id`,
 1 AS `registered_at`,
 1 AS `last_updated_at`,
 1 AS `deleted_at`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `variants`
--

DROP TABLE IF EXISTS `variants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `variants` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `product_id` bigint(20) unsigned NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `size_id` int(10) unsigned NOT NULL DEFAULT '0',
  `stock` int(10) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `primary` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `legacy_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `variant_product_stock_index` (`deleted_at`,`stock`,`product_id`) USING BTREE,
  KEY `variants_code_index` (`code`) USING BTREE,
  KEY `variants_code_primary_status_index` (`code`,`primary`,`status`) USING BTREE,
  KEY `variants_code_status_index` (`code`,`status`) USING BTREE,
  KEY `variants_id_stock_status_index` (`id`,`stock`,`status`) USING BTREE,
  KEY `variants_product_id_status_index` (`product_id`,`status`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `variants`
--

LOCK TABLES `variants` WRITE;
/*!40000 ALTER TABLE `variants` DISABLE KEYS */;
/*!40000 ALTER TABLE `variants` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'monoposdb_v4'
--

--
-- Final view structure for view `v4_places`
--

/*!50001 DROP VIEW IF EXISTS `v4_places`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v4_places` AS select `p`.`ec_id` AS `client_id`,`p`.`id` AS `id`,`p`.`name` AS `name`,`p`.`description` AS `description`,`a`.`postcode` AS `postal_code`,`a`.`address` AS `address`,`a`.`name` AS `address_name`,`a`.`phone_number` AS `contact_phone_number`,`p`.`created_at` AS `registered_at`,`p`.`updated_at` AS `last_updated_at`,`p`.`deleted_at` AS `deleted_at` from (`places` `p` left join `shipping_informations` `a` on((`p`.`id` = `a`.`place_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v4_skus`
--

/*!50001 DROP VIEW IF EXISTS `v4_skus`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v4_skus` AS select `v4_skus_dev`.`client_id` AS `client_id`,`v4_skus_dev`.`id` AS `id`,`v4_skus_dev`.`bar_code` AS `bar_code`,`v4_skus_dev`.`variant_id` AS `variant_id`,`v4_skus_dev`.`registered_at` AS `registered_at`,`v4_skus_dev`.`last_updated_at` AS `last_updated_at`,`v4_skus_dev`.`deleted_at` AS `deleted_at` from `v4_skus_dev` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v4_skus_prd`
--

/*!50001 DROP VIEW IF EXISTS `v4_skus_prd`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v4_skus_prd` AS select `p`.`ec_id` AS `client_id`,`v`.`id` AS `id`,`v`.`code` AS `bar_code`,`v`.`id` AS `variant_id`,`v`.`created_at` AS `registered_at`,`v`.`updated_at` AS `last_updated_at`,`v`.`deleted_at` AS `deleted_at` from (`variants` `v` left join `products` `p` on((`v`.`product_id` = `p`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v4_variants`
--

/*!50001 DROP VIEW IF EXISTS `v4_variants`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v4_variants` AS select `p`.`ec_id` AS `client_id`,`v`.`id` AS `id`,`v`.`product_id` AS `product_id`,`v`.`created_at` AS `registered_at`,`v`.`updated_at` AS `last_updated_at`,`v`.`deleted_at` AS `deleted_at` from (`variants` `v` left join `products` `p` on((`v`.`product_id` = `p`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-15 15:33:36
