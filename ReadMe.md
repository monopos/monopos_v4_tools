# **リファクタリング2020プロジェクト - ツールキット**

下記を含みます。

- **[monopos-db](https://bitbucket.org/monopos/monopos_v4_tools/src/master/monopos-db/)** ... データベース・スキーマ定義とERD
- **[data-migration](https://bitbucket.org/monopos/monopos_v4_tools/src/master/data-migration/)** ... データ移行スクリプト

（※2020年8月26日時点）
